import { useState } from "react";
import { SentimentNeutral } from "@mui/icons-material";
import "./app.css";

function App() {
  const [signupFormData, setSignupFormData] = useState<{
    name: string;
    email: string;
    password: string;
  }>({ name: "", email: "", password: "" });

  const handleChange = (e: any, inputName: string) =>
    setSignupFormData({ ...signupFormData, [inputName]: e.value });

  const onSubmit = () => {
    console.log({ results: signupFormData });
  };

  return (
    <div className="app">
      <div className="circle"></div>
      <div className="card">
        <div className="logo">
          <SentimentNeutral fontSize="large" />
        </div>
        <h2>Create Account</h2>
        <form onSubmit={(e) => e.preventDefault()} className="form">
          <input
            type="text"
            placeholder="Name"
            onChange={(e) => handleChange(e, "name")}
          />
          <input
            type="email"
            placeholder="Email"
            onChange={(e) => handleChange(e, "email")}
          />
          <input
            type="password"
            placeholder="Password"
            onChange={(e) => handleChange(e, "password")}
          />
          <button onClick={onSubmit}>Sign Up</button>
        </form>
        <div className="footer">
          Existing users, sign in
          <a href="#">Here</a>
        </div>
      </div>
    </div>
  );
}

export default App;
